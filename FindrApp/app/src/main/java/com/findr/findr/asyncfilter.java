package com.findr.findr;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RAJCHELCX1 on 12/12/2017.
 */

public class asyncfilter extends AsyncTask<ArrayList<Person>, Void, ArrayList<Person>> {

    public asyncdelegate d;
    public Context c;
    public int r;
    public ArrayList<Person> toShow;
    public Geocoder gc;

    asyncfilter(asyncdelegate d,Context c,int r){
        this.d = d;
        this.c = c;
        this.r = r;
    }

    @Override
    protected ArrayList<Person> doInBackground(ArrayList<Person>[] persons) {
        //Sets user location to california...
        Location userLocation = new Location("userlocation");
        userLocation.setLatitude(32.7157);
        userLocation.setLongitude(-117.1611);
        Location personLocation = new Location("newlocation");

        ArrayList<Pair<String,Boolean>>  placesCalculated = new ArrayList<Pair<String,Boolean>>();
        toShow = new ArrayList<Person>();
        gc = new Geocoder(c);

        //For every person who is missing...
        for (int i = 0; i < persons[0].size(); i++) {
            try {
                //Get a string for the current persons location...
                String personLocationStr = persons[0].get(i).city + ", " + persons[0].get(i).state;
                Pair<String,Boolean> inLoc = new Pair(personLocationStr,true);
                Pair<String,Boolean> outLoc = new Pair(personLocationStr,false);

                //If the current location has never been calculated before...
                if(!placesCalculated.contains(inLoc)&&!placesCalculated.contains(outLoc)) {
                    List<Address> addr = gc.getFromLocationName(personLocationStr, 1);
                    if (!addr.isEmpty()) {
                        personLocation.setLatitude(addr.get(0).getLatitude());
                        personLocation.setLongitude(addr.get(0).getLongitude());
                        double distance = userLocation.distanceTo(personLocation) * 0.00062137;
                        if (distance > r) {
                            Pair<String, Boolean> out = new Pair(personLocationStr, false);
                            placesCalculated.add(out);
                        } else {
                            Pair<String, Boolean> in = new Pair(personLocationStr, true);
                            placesCalculated.add(in);
                        }
                    }
                }

                if(placesCalculated.contains(inLoc)) {
                    toShow.add(persons[0].get(i));
                }

            } catch (Exception e) {
                System.out.println(e.toString());
            }
        }
        return toShow;
    }

    @Override
    protected void onPostExecute(ArrayList<Person> people) {
        super.onPostExecute(people);
        d.updateList(toShow);
    }
}
