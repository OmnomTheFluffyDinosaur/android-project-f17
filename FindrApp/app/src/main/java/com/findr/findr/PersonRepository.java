package com.findr.findr;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by GREATHOUSEMS1 on 11/13/2017.
 */

public class PersonRepository {
    private static PersonRepository instance;
    private IPersonRepository delegate;

    private Context context;
    public boolean isLoaded;
    public ArrayList<Person> persons;

    public PersonRepository(Context c) {
        persons = new ArrayList<Person>();
        isLoaded = false;
        context = c;
    }

    public void loadData(IPersonRepository d) {
        delegate = d;
        if (isLoaded) {
            delegate.loadComplete();
            return;
        }
        /* FORMAT URL FOR REQUEST */
        RequestQueue queue = Volley.newRequestQueue(context);
        String url = "http://find-us.herokuapp.com";
                /* LOAD JSON REQUEST FROM URL */
        final JsonArrayRequest jRequest = new JsonArrayRequest (Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        for (int i = 0; i < response.length(); i++) {
                            try {
                                        /* CREATE A PERSON FOR EACH RESULT */
                                Person P = new Person();
                                JSONObject obj = response.getJSONObject(i);
                                P.firstName = obj.getString("first_name");
                                P.lastName = obj.getString("last_name");
                                P.city = obj.getString("city");
                                P.county = obj.getString("county");
                                P.country = obj.getString("country");
                                P.state = obj.getString("state");
                                persons.add(P);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        delegate.loadComplete();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error.toString());
            }
        }) {
        };

                /* ADD REQUEST TO MAIN QUEUE */
        queue.add(jRequest);
        isLoaded = true;
    }

    public void add(Person p) {
        persons.add(p);
    }

    public ArrayList<Person> getStrings() {
        return new ArrayList<Person>(persons);
    }

    public static synchronized PersonRepository getInstance(Context c) {
        if (instance == null) {
            instance = new PersonRepository(c);
        }
        return instance;
    }
}
