package com.findr.findr;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GREATHOUSEMS1 on 11/13/2017.
 */

public class PersonAdapter extends BaseAdapter implements Filterable {
    private Context mContext;
    private ArrayList<Person> persons;
    private ArrayList<Person> mStringFilterList;
    public ValueFilter valueFilter;

    public PersonAdapter(Context c, ArrayList<Person> personList) {
        mContext = c;
        persons = personList;
        mStringFilterList = persons;
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int i) {
        return persons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.person_layout,parent, false);
        }

        TextView personNameText = (TextView) convertView.findViewById(R.id.personName);
        TextView personLocationText = (TextView) convertView.findViewById(R.id.personLocation);

        String fullName = persons.get(position).firstName + " " + persons.get(position).lastName;
        String location = persons.get(position).city;

        personNameText.setText(fullName);
        personLocationText.setText(location);

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            //Convert the query into lowercase format
            String query = constraint.toString().toLowerCase();

            //If the search query isn't empty
            if (constraint != null && constraint.length() > 0) {
                //An array list to hold the query results
                ArrayList<Person> filterList = new ArrayList<Person>();
                for (int i = 0; i < mStringFilterList.size(); i++) {
                    Person p = mStringFilterList.get(i);
                    //Determine if the query matches a person based on city, state, firstname, lastname and add to filterList if so...
                    if (p.city.toLowerCase().contains(query)) {
                        filterList.add(p);
                    } else if (p.state.toLowerCase().contains(query)) {
                        filterList.add(p);
                    } else if (p.firstName.toLowerCase().contains(query)) {
                        filterList.add(p);
                    } else if (p.lastName.toLowerCase().contains(query)) {
                        filterList.add(p);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mStringFilterList.size();
                results.values = mStringFilterList;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            persons = (ArrayList<Person>) results.values;
            notifyDataSetChanged();
        }

    }
}
