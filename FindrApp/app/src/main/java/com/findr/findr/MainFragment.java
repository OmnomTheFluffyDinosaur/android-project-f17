package com.findr.findr;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class MainFragment extends Fragment implements IPersonRepository, asyncdelegate{

    private ArrayList<Person> persons;
    public ListView personList;
    public PersonRepository personRepo;
    public SharedPreferences sharedPref;
    public int radius;
    public PersonAdapter adapter;
    public SwipeRefreshLayout mSwipeRefresh;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public MainFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MainFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);

        personList = (ListView) v.findViewById(R.id.personListView);
        mSwipeRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        mSwipeRefresh.setRefreshing(true);

        sharedPref = getActivity().getSharedPreferences("settings", Context.MODE_PRIVATE);
        radius = sharedPref.getInt("radius", 10);
        System.out.println(radius);
        getActivity().setTitle("Findr - " + radius + " mile radius");

        persons = new ArrayList<Person>();

        //put data from person repo in the arraylist
        personRepo = PersonRepository.getInstance(this.getActivity());
        personRepo.loadData(this);

        Person joe = new Person();
        joe.firstName = "Joe";
        joe.lastName = "Johnson";
        joe.city = "Sacramento";
        joe.state = "California";

       personList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(),MapsActivity.class);
                i.putExtra("city",persons.get(position).city);
                i.putExtra("state",persons.get(position).state);
                startActivity(i);
            }
        });

        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                System.out.println("Refreshing...");
                refresh();
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void loadComplete() {
        persons = personRepo.persons;

        //Begin filtering the entire json dataset...
        asyncfilter myf = new asyncfilter(this,getContext(),radius);
        myf.execute(persons);
    }

    public void updateList(ArrayList<Person> res) {
        adapter = new PersonAdapter(getActivity(), res);
        personList.setAdapter(adapter);
        mSwipeRefresh.setRefreshing(false);
    }

    private void refresh() {
        personRepo.isLoaded = false;
        personRepo.loadData(this);
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
