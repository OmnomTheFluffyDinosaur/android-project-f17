package com.findr.findr;

import java.util.Date;

/**
 * Created by GREATHOUSEMS1 on 11/8/2017.
 */

public class Person {
    int age;
    String firstName;
    String lastName;
    String city;
    String state;
    String country;
    String county;
    Date date;
    String eyeColor;
    String hairColor;
    String height;
    String weight;
    String sex;
    String race;
}
